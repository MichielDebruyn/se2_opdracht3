package be.kdg.delidish.business;

import be.kdg.delidish.business.domain.order.Order;
import be.kdg.delidish.business.domain.order.OrderState;
import be.kdg.delidish.business.domain.restaurant.Restaurant;
import be.kdg.delidish.business.services.DistanceService;
import be.kdg.delidish.persistence.*;
import be.kdg.delidish.business.domain.person.*;

import java.util.Iterator;
import java.util.List;

public class OrderManager {

	private final OrderRepository orderRepository;
	private final CourierRepository courierRepository;
	private final RestaurantRepository restaurantRepository;

	public OrderManager(OrderRepository orderRepository, CourierRepository courierRepository, RestaurantRepository restaurantRepository) {
		this.orderRepository = orderRepository;
		this.courierRepository = courierRepository;
		this.restaurantRepository = restaurantRepository;
	}

	/**
	 * 
	 * @param id
	 * @param courierId
	 */
	public void assignOrder(int id, int courierId) {
		Order order = orderRepository.get(id);
		order.assignCourier(this.courierRepository.get(courierId));
		order.addEvent(OrderState.ACCEPTED_BY_COURIER);
		orderRepository.update(order);
	}

	/**
	 *
	 * @param courierId
	 */
	public List<Order> getAvailableOrders(int courierId) {
		List<Order> availableOrders =  this.orderRepository.getOrdersByOrderStateAndMaxDeliveryTime(OrderState.DISHES_PREPARED);

		for (Order order : availableOrders) {
			Courier courier = this.courierRepository.get(courierId);
			//neemt het eerste gerecht uit de eerste orderline en controleert zo het restaurant
			Restaurant restaurant = this.restaurantRepository.getRestaurantByDish(order.getOrderLines().get(0).getDishes().get(0));

			int totalDeliveryTime = DistanceService.getDistanceTimeFromCourierToRestaurant(courier, restaurant) +
					DistanceService.getDistanceTimeFromRestaurantToOrderDeliveryAddress(restaurant, order);

				availableOrders.removeIf(currentOrder -> currentOrder.getMaximumDeliveryTime() > totalDeliveryTime);
		}

		return availableOrders;
	}
}