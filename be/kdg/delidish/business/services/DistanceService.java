package be.kdg.delidish.business.services;

import be.kdg.delidish.business.domain.order.Order;
import be.kdg.delidish.business.domain.person.Courier;
import be.kdg.delidish.business.domain.restaurant.Restaurant;

public class DistanceService {
    public static int getDistanceTimeFromCourierToRestaurant(Courier courier, Restaurant restaurant){
        int cx = courier.getCurrentPosition().getLongitude();
        int cy = courier.getCurrentPosition().getLattitude();

        int rx = restaurant.getContactInfo().getAddress().getPosition().getLongitude();
        int ry = restaurant.getContactInfo().getAddress().getPosition().getLattitude();

        return calculateLine(cx, rx, cy, ry);
    }

    public static int getDistanceTimeFromRestaurantToOrderDeliveryAddress(Restaurant restaurant, Order order) {
        int rx = restaurant.getContactInfo().getAddress().getPosition().getLongitude();
        int ry = restaurant.getContactInfo().getAddress().getPosition().getLattitude();

        int ox = order.getDeliveryAddress().getPosition().getLongitude();
        int oy = order.getDeliveryAddress().getPosition().getLattitude();

        return calculateLine(rx, ox, ry, oy);
    }

    private static int calculateLine(int x1, int x2, int y1, int y2) {
        return (int) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    }
}
