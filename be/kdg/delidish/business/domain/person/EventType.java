package be.kdg.delidish.business.domain.person;

public enum EventType {
	;

	private int MISSION_ACCEPTED;
	private int TIMELY_PICKUP;
	private int LATE_PICKUP;
	private int TIMELY_DELIVERY;
	private int LATE_DELIVERY;
	private int DAILY_REDUCTION;

}