package be.kdg.delidish.business.domain.restaurant;

import java.util.*;
import be.kdg.delidish.business.domain.common.*;

public class Restaurant {

	private List<Dish> dishes;
	private String name;
	private ContactInfo contactInfo;
	private OpeningPeriod[] openingHours;
	private String bankAccountNr;

	public ContactInfo getContactInfo() {
		return contactInfo;
	}

	public List<Dish> getDishes() {
		return dishes;
	}
}