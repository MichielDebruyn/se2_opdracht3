package be.kdg.delidish.business.domain.common;

public class Position {

	private int longitude;
	private int lattitude;

	public int getLongitude() {
		return longitude;
	}

	public int getLattitude() {
		return lattitude;
	}
}