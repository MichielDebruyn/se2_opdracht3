package be.kdg.delidish.business.domain.common;

public class ContactInfo {

	private Address address;
	private String email;
	private String tel;

	public Address getAddress() {
		return address;
	}
}