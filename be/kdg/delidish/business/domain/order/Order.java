package be.kdg.delidish.business.domain.order;

import java.util.*;
import be.kdg.delidish.business.domain.person.*;
import be.kdg.delidish.business.domain.common.*;
import be.kdg.delidish.business.domain.restaurant.Dish;

public class Order {

	private List<OrderlLine> orderLines;
	private List<OrderEvent> orderEvents;
	Customer customer;
	Courier courier;
	private int orderId;
	private Address deliveryAddress;
	private String deliveryInstructions;
	private int averageDeliveryPoints;
	private OrderState state;

	/**
	 * 
	 * @param courier
	 */
	public void assignCourier(Courier courier) {
		
		
	}

	/**
	 * 
	 * @param os
	 */
	public void addEvent(OrderState os) {
		
		
	}

	public int getOrderId() {
		return orderId;
	}

	public OrderState getState() {
		return state;
	}

	public int getMaximumDeliveryTime() {
		int shortestDeliveryTime = 0;
		for (OrderlLine orderLine : orderLines) {
			for (Dish dish : orderLine.getDishes()) {
				if (shortestDeliveryTime == 0) {
					shortestDeliveryTime = dish.getMaximumDeliveryTime();
				}
				else if (dish.getMaximumDeliveryTime() < shortestDeliveryTime) {
					shortestDeliveryTime = dish.getMaximumDeliveryTime();
				}
			}
		}
		return shortestDeliveryTime;
	}

	public int getCalculatedDistanceTime(int distance) {
		return (int) distance*4;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public OrderlLine getOrdelineByIndexValue(int indexValue) {
		return this.orderLines.get(indexValue);
	}

	public List<OrderlLine> getOrderLines() {
		return orderLines;
	}
}