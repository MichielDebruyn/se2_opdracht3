package be.kdg.delidish.persistence;

import be.kdg.delidish.business.domain.restaurant.Dish;
import be.kdg.delidish.business.domain.restaurant.Restaurant;

import java.util.Map;

public class RestaurantRepository {

    Map<Integer, Restaurant> restaurantRepositoryMap;

    public Restaurant getRestaurantByDish(Dish dish) {

        for (Map.Entry<Integer, Restaurant> entry: restaurantRepositoryMap.entrySet()) {
            if (entry.getValue().getDishes().contains(dish)) {
                return entry.getValue();
            }
        }

        return null;
    }
}
