package be.kdg.delidish.persistence;

import be.kdg.delidish.business.domain.order.*;
import be.kdg.delidish.business.domain.person.Courier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderRepository {

	Map<Integer, Order> orderRepositoryMap;
	/**
	 * 
	 * @param id
	 */
	public Order get(int id) {
		return orderRepositoryMap.get(id);
	}

	/**
	 * 
	 * @param order
	 */
	public void update(Order order) {
		orderRepositoryMap.put(order.getOrderId(), order);
	}

	public List<Order> getOrdersByOrderState(OrderState orderState) {
		List<Order> availableOrders = new ArrayList<>();
		for (Map.Entry<Integer, Order> entry: orderRepositoryMap.entrySet()) {
			if (entry.getValue().getState().equals(orderState)) {
				availableOrders.add(entry.getValue());
			}
		}
		return availableOrders;
	}

	public List<Order> getOrdersByOrderStateAndMaxDeliveryTime(OrderState orderState) {
		List<Order> availableOrders = new ArrayList<>();
		for (Map.Entry<Integer, Order> entry: orderRepositoryMap.entrySet()) {
			if (entry.getValue().getState().equals(orderState)) {
				availableOrders.add(entry.getValue());
			}
		}
		return availableOrders;
	}
}