package be.kdg.delidish.application;

import be.kdg.delidish.business.*;
import be.kdg.delidish.business.domain.order.Order;
import be.kdg.delidish.business.domain.person.Courier;

import java.util.List;

public class DeliveryController {

	private final OrderManager orderManager;
	private final UserManager userManager;

	public DeliveryController(OrderManager orderManager, UserManager userManager) {
		this.orderManager = orderManager;
		this.userManager = userManager;
	}

	/**
	 * 
	 * @param orderId
	 * @param courier
	 */
	public void selectDelivery(int orderId,Courier courier) {
		orderManager.assignOrder(orderId, courier);
	}

	/**
	 *
	 * @param courierId
	 */
	public List<Order> showAvailableOrders(int courierId) {
		return this.orderManager.getAvailableOrders(courierId);
	}
}